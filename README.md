# Lmax Disruptor Usage

For more look at [NiezawodnyKod.pl](https://niezawodnykod.pl)

### Usage
Before running [ApplicationUsingLmaxDisruptor](src/main/java/pl/niezawodnykod/lmax/disruptor/ApplicationUsingLmaxDisruptor.java)
run:
> docker-compose up

To have `grafana` dashboard available to monitor the amount of messages flowing through the example.