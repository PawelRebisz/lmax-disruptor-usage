package pl.niezawodnykod.lmax.disruptor.model;

import lombok.Getter;
import lombok.Setter;

public class DataTransferEvent {
    @Getter
    @Setter
    private long value;
}
