package pl.niezawodnykod.lmax.disruptor.core;

public interface BetweenProcessTransport {
    void send(long message);
    void start();
}
