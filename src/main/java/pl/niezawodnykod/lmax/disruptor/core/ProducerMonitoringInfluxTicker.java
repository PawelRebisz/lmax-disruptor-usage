package pl.niezawodnykod.lmax.disruptor.core;

import lombok.RequiredArgsConstructor;
import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;
import org.influxdb.dto.Point;

import java.util.List;
import java.util.concurrent.TimeUnit;

@RequiredArgsConstructor
public class ProducerMonitoringInfluxTicker implements Runnable {

    private final List<DefaultEventProducer> producers;
    private final List<DefaultEventConsumer> consumers;

    @Override
    public void run() {
        final String serverURL = "http://127.0.0.1:8086", username = "user", password = "password";
        final InfluxDB influxDB = InfluxDBFactory.connect(serverURL, username, password);
        influxDB.setDatabase("monitoring");

        long startTimestamp = System.currentTimeMillis();
        while (true) {
            Point point = pointOfMeasurement(startTimestamp);
            influxDB.write(point);
            System.out.println(point.toString());
            try {
                Thread.sleep(1_000L);

            } catch (InterruptedException exception) {
                throw new RuntimeException(exception);
            }
        }
    }

    private Point pointOfMeasurement(long startTimestamp) {
        long numberOfMessagesSend = producers.stream()
                .map(DefaultEventProducer::getCurrentMessagesCount)
                .reduce(0L, Long::sum);
        long numberOfMessagesConsumed = consumers.stream()
                .map(DefaultEventConsumer::getCurrentMessagesCount)
                .reduce(0L, Long::sum);
        System.out.println(numberOfMessagesSend + " -> " + numberOfMessagesConsumed);
        Point.Builder performance = Point.measurement("performance")
                .time(System.currentTimeMillis(), TimeUnit.MILLISECONDS)
                .addField("msgSend", throughputFrom(startTimestamp, numberOfMessagesSend))
                .addField("msgConsumed", throughputFrom(startTimestamp, numberOfMessagesConsumed));
        producers.forEach(producer -> performance.addField(
                producer.getProducerName() + ".send",
                throughputFrom(startTimestamp, producer.getCurrentMessagesCount())
        ));
        consumers.forEach(consumer -> performance.addField(
                consumer.getConsumerName() + ".consumed",
                throughputFrom(startTimestamp, consumer.getCurrentMessagesCount())
        ));
        return performance.build();
    }

    private long throughputFrom(long timestamp, long messages) {
        return messages / (System.currentTimeMillis() - timestamp) * 1000;
    }
}
