package pl.niezawodnykod.lmax.disruptor.core;

import com.lmax.disruptor.EventHandler;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import pl.niezawodnykod.lmax.disruptor.model.DataTransferEvent;

import java.util.concurrent.atomic.AtomicLong;

@RequiredArgsConstructor
public class DefaultEventConsumer implements EventHandler<DataTransferEvent> {

    private final AtomicLong numberOfMessagesConsumed = new AtomicLong(0);
    @Getter
    private final String consumerName;

    public void onEvent(DataTransferEvent event, long sequence, boolean endOfBatch) {
        process(event);
    }

    public void process(DataTransferEvent event) {
        long value = event.getValue();
        numberOfMessagesConsumed.set(numberOfMessagesConsumed.incrementAndGet());
    }

    public long getCurrentMessagesCount() {
        return numberOfMessagesConsumed.get();
    }
}
