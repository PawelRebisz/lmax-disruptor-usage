package pl.niezawodnykod.lmax.disruptor.core;

import com.lmax.disruptor.EventFactory;
import pl.niezawodnykod.lmax.disruptor.model.DataTransferEvent;

public class DefaultEventFactory implements EventFactory<DataTransferEvent> {
    public DataTransferEvent newInstance() {
        return new DataTransferEvent();
    }
}
