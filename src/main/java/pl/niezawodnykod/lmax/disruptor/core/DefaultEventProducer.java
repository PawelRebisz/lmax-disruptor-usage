package pl.niezawodnykod.lmax.disruptor.core;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;

@RequiredArgsConstructor
public class DefaultEventProducer implements Runnable {

    private final AtomicLong numberOfMessagesSent = new AtomicLong(0);
    @Getter
    private final String producerName;
    private final BetweenProcessTransport transport;
    private final Random random = new Random();

    public long getCurrentMessagesCount() {
        return numberOfMessagesSent.get();
    }

    public void run() {
        while (true) {
            transport.send(random.nextLong());
            numberOfMessagesSent.set(numberOfMessagesSent.incrementAndGet());
        }
    }
}
