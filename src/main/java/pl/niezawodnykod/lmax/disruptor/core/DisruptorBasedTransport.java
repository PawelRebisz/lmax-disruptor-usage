package pl.niezawodnykod.lmax.disruptor.core;

import com.lmax.disruptor.BusySpinWaitStrategy;
import com.lmax.disruptor.RingBuffer;
import com.lmax.disruptor.dsl.Disruptor;
import com.lmax.disruptor.dsl.ProducerType;
import com.lmax.disruptor.util.DaemonThreadFactory;
import pl.niezawodnykod.lmax.disruptor.model.DataTransferEvent;

public class DisruptorBasedTransport implements BetweenProcessTransport {

    private final Disruptor<DataTransferEvent> disruptor;
    private RingBuffer<DataTransferEvent> ringBuffer;
    private DefaultEventConsumer[] handlers;

    public DisruptorBasedTransport(int transportQueueSlots, DefaultEventConsumer... handlers) {
        disruptor = new Disruptor<>(
                new DefaultEventFactory(),
                transportQueueSlots,
                DaemonThreadFactory.INSTANCE,
                ProducerType.SINGLE,
                new BusySpinWaitStrategy()
        );
        this.handlers = handlers;
    }

    @Override
    public void start() {
        disruptor.handleEventsWith(handlers);
        ringBuffer = disruptor.start();
    }

    @Override
    public void send(long message) {
        long sequence = ringBuffer.next();
        try {
            DataTransferEvent valueEvent = ringBuffer.get(sequence);
            valueEvent.setValue(message);

        } finally {
            ringBuffer.publish(sequence);
        }
    }
}
