package pl.niezawodnykod.lmax.disruptor.core;

import pl.niezawodnykod.lmax.disruptor.model.DataTransferEvent;

import java.util.Arrays;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static java.util.Objects.isNull;

public class BlockingQueueBasedTransport implements BetweenProcessTransport {

    private final ArrayBlockingQueue<DataTransferEvent> queue;
    private final DefaultEventConsumer[] handlers;

    public BlockingQueueBasedTransport(int transportQueueSlots, DefaultEventConsumer... handlers) {
        this.queue = new ArrayBlockingQueue<>(transportQueueSlots);
        this.handlers = handlers;
    }

    @Override
    public void start() {
        ExecutorService executorService = Executors.newFixedThreadPool(handlers.length);
        Arrays.asList(handlers)
                .forEach(handler -> executorService.execute(eventFetchingWrapperAround(handler)));
    }

    @Override
    public void send(long message) {
        DataTransferEvent valueEvent = new DataTransferEvent();
        valueEvent.setValue(message);
        try {
            queue.put(valueEvent);
        } catch (InterruptedException exception) {
            throw new RuntimeException("Something went wrong here", exception);
        }
    }

    private Runnable eventFetchingWrapperAround(DefaultEventConsumer handler) {
        return () -> {
            while (true) {
                try {
                    DataTransferEvent take = queue.poll(100L, TimeUnit.MILLISECONDS);
                    if (!isNull(take)) {
                        handler.process(take);
                    }
                } catch (InterruptedException exception) {
                    throw new RuntimeException("Something went wrong here", exception);
                }
            }
        };
    }
}
