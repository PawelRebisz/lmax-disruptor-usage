package pl.niezawodnykod.lmax.disruptor;

import picocli.CommandLine.Option;

public class CliCommand {
    @Option(names = {"-p", "--producers"}, description = "number of producers", defaultValue = "1")
    int numberOfProducers;

    @Option(names = {"-c", "--consumers"}, description = "number of consumers", defaultValue = "3")
    int numberOfConsumers;

    @Option(names = {"-t", "--transport-type"}, description = "type of transport [disruptor|queue]", defaultValue = "queue")
    String transportType;
}
