package pl.niezawodnykod.lmax.disruptor;

import picocli.CommandLine;
import pl.niezawodnykod.lmax.disruptor.core.BetweenProcessTransport;
import pl.niezawodnykod.lmax.disruptor.core.BlockingQueueBasedTransport;
import pl.niezawodnykod.lmax.disruptor.core.DefaultEventProducer;
import pl.niezawodnykod.lmax.disruptor.core.DisruptorBasedTransport;
import pl.niezawodnykod.lmax.disruptor.core.ProducerMonitoringInfluxTicker;
import pl.niezawodnykod.lmax.disruptor.core.DefaultEventConsumer;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ApplicationUsingLmaxDisruptor {

    public static final int TRANSPORT_QUEUE_SLOTS = 8192;

    public static void main(String[] args) {
        CliCommand command = new CliCommand();
        new CommandLine(command).parseArgs(args);

        DefaultEventConsumer[] consumers = IntStream.range(1, command.numberOfConsumers + 1)
                .mapToObj(value -> new DefaultEventConsumer("consumer" + value))
                .toArray(DefaultEventConsumer[]::new);
        BetweenProcessTransport transport = getBetweenProcessTransport(command, consumers);
        transport.start();

        ExecutorService executorService = Executors.newFixedThreadPool(command.numberOfProducers + 1);
        List<DefaultEventProducer> producers = IntStream.range(1, command.numberOfProducers + 1)
                .mapToObj(value -> new DefaultEventProducer("producer" + value, transport))
                .collect(Collectors.toList());

        producers.forEach(executorService::execute);
        executorService.execute(new ProducerMonitoringInfluxTicker(producers, Arrays.asList(consumers)));
    }

    private static BetweenProcessTransport getBetweenProcessTransport(CliCommand command,
                                                                      DefaultEventConsumer[] handlers) {
        switch (command.transportType) {
            case "queue":
                return new BlockingQueueBasedTransport(TRANSPORT_QUEUE_SLOTS, handlers);
            case "disruptor":
                return new DisruptorBasedTransport(TRANSPORT_QUEUE_SLOTS, handlers);
            default:
                throw new IllegalArgumentException("Failed to recognize transport type: " + command.transportType);
        }
    }
}
